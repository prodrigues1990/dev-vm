# VirtualBox Dev Machine

Use VirtualBox, Vagrant, and Ansible to provision a dev virtual machine with the
following software:

 * vim
 * git
 * python3
 * docker
 * mongodb and robo3t
 * chromium
 * firefox
 * vscode

## Boot

Make sure you have:

 * [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
 * [Vagrant](https://www.vagrantup.com/downloads.html)
 * [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

Run `vagrant up`. It may take a while depending on your internet connection.
